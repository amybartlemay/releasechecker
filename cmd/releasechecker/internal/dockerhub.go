package internal

import (
	"fmt"
	"github.com/heroku/docker-registry-client/registry"
)

type dockerHubImage struct {
	group string
	name  string
}

// DockerHubClient client
type DockerHubClient struct {
	docker *registry.Registry
}

// NewDockerHubClient Creates dockerHubClient
func NewDockerHubClient() *DockerHubClient {
	huburl := "https://registry-1.docker.io/"
	username := "" // anonymous
	password := "" // anonymous

	hub, err := registry.New(huburl, username, password)

	if err != nil {
		log.Errorf("Failed to create DockerHub registry client: %s", err)
		return nil
	}

	// Change the registry logger
	hub.Logf = log.Infof

	return &DockerHubClient{
		hub,
	}
}

func (c *DockerHubClient) checkDockerHubNewTags(b *Bot) bool {
	log.Infof("Checking new DockerHub tags...")

	images, err := gDB.GetDockerHubConfiguredImagesToCheck()
	if err != nil {
		log.Errorf("Failed to fetch DockerHub configured images")
		return false
	}

	if err := c.docker.Ping(); err != nil {
		log.Errorf("Failed to ping dockerhub: %s", err)
		return false
	}

	for _, image := range images {
		log.Infof("Fetching DockerHub image %s/%s tags", image.group, image.name)
		tags, err := c.docker.Tags(fmt.Sprintf("%s%%2F%s", image.group, image.name))
		if err != nil {
			log.Errorf("Failed to list DockerHub image tags: %s", err)
			continue
		}

		rb := NewReleaseStringBuilder(
			fmt.Sprintf("**%s/%s** docker image tags", image.group, image.name),
		)

		newTags := make([]string, 0)
		for _, t := range tags {
			// Ignore latest tag
			if t == "latest" {
				continue
			}

			registered, err := gDB.IsDockerHubImageTagRegistered(image.group, image.name, t)
			if err != nil {
				return false
			}

			if !registered {
				newTags = append(newTags, t)
				rb.Append(fmt.Sprintf("* [%s](https://hub.docker.com/r/%s/%s/tags)", t, image.group, image.name))
			}
		}

		if !rb.Empty() {
			if !b.DispatchEvent(rb.String()) {
				return false
			}

			for _, t := range newTags {
				gDB.RegisterDockerHubImageTag(image.group, image.name, t)
			}
		}

		gDB.SetLastDockerhubImageUpdate(image.group, image.name)
	}

	log.Infof("New DockerHub tags fetch done.")
	return true
}
