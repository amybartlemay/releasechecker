package internal

import (
	"fmt"
	"strings"
)

// ReleaseStringBuilder object
type ReleaseStringBuilder struct {
	buffer strings.Builder
	empty  bool
}

// NewReleaseStringBuilder creates ReleaseStringBuilder
func NewReleaseStringBuilder(t string) *ReleaseStringBuilder {
	r := &ReleaseStringBuilder{}
	r.Append(fmt.Sprintf("New %s released!\n", t))
	r.empty = true
	return r
}

// Append add strings to the buffer
func (r *ReleaseStringBuilder) Append(s string) {
	r.buffer.WriteString(fmt.Sprintf("%s\n", s))
	r.empty = false
}

// String returns the string
func (r *ReleaseStringBuilder) String() string {
	return r.buffer.String()
}

// Empty verify if release string is empty
func (r *ReleaseStringBuilder) Empty() bool {
	return r.empty
}
