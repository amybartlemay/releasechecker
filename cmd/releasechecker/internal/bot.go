package internal

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/robfig/cron/v3"
	lm "gitlab.com/nerzhul/libmatterbot"
)

// Bot bot type
type Bot struct {
	// mattermost slashcommand handler
	webhookHandler *lm.Listener
	cron           *cron.Cron
	mClient        *lm.Client
	shouldStop     bool
}

// NewBot Create bot object
func NewBot() (b *Bot) {
	b = &Bot{
		cron:           cron.New(),
		webhookHandler: lm.NewListener(&gconfig.Bot),
		mClient:        lm.NewClient(&gconfig.Bot),
		shouldStop:     false,
	}

	return b
}

// Start bot
func (b *Bot) Start() {
	_, err := b.cron.AddFunc(gconfig.Schedule, func() {
		NewGithubClient().checkGithubNewTags(b)
	})

	if err != nil {
		log.Fatalf("Unable to schedule releasechecker cron: %s", err)
	}

	_, err = b.cron.AddFunc(gconfig.Schedule, func() {
		dc := NewDockerHubClient()
		if dc != nil {
			dc.checkDockerHubNewTags(b)
		}
	})

	if err != nil {
		log.Fatalf("Unable to schedule releasechecker cron: %s", err)
	}

	log.Infof("Release schedule set to: %s", gconfig.Schedule)

	b.cron.Start()
	b.setupWebhookHandler()

	if !b.mClient.IsMattermostUp() {
		log.Warning("Unable to notify startup to mattermost instance.")
	} else {
		b.mClient.Post(gconfig.Mattermost.ReleaseAnnouncementsChannel,
			fmt.Sprintf("ReleaseChecker (rev. %s) application has started.", AppVersion))
	}
}

// Stop bot
func (b *Bot) Stop() {
	b.shouldStop = true
	b.cron.Stop()

	if !b.mClient.IsMattermostUp() {
		log.Warning("Unable to notify shutdown to mattermost instance.")
	} else {
		b.mClient.Post(gconfig.Mattermost.ReleaseAnnouncementsChannel,
			fmt.Sprintf("ReleaseChecker (rev. %s) application has stopped normally.", AppVersion))
	}

	b.mClient.Deinit()
}

// DispatchEvent dispatch release events
func (b *Bot) DispatchEvent(ev string) bool {
	if !b.mClient.IsMattermostUp() {
		log.Errorf("Github: failed to ping mattermost.")
		return false
	}

	if !b.mClient.Post(gconfig.Mattermost.ReleaseAnnouncementsChannel, ev) {
		log.Errorf("Github: Failed to publish Post on mattermost.")
		return false
	}

	return true
}

func (b *Bot) setupWebhookHandler() {
	// initialize port & command handler
	b.webhookHandler.SetAllowedToken(gconfig.Bot.HTTP.AllowedToken)
	b.webhookHandler.RegisterHealthcheck("/health", b.handleHealthcheck)
	b.webhookHandler.RegisterHealthcheck("/ready", b.handleReadinessCheck)
	b.webhookHandler.RegisterSlashCommandHandler("/checkrelease", lm.SlashCommandHandler{
		Handle:    b.handleCheckReleaseSlashCommand,
		SplitArgs: true,
		MinArgs:   3,
		MaxArgs:   3,
	})
	b.webhookHandler.RegisterSlashCommandHandler("/uncheckrelease", lm.SlashCommandHandler{
		Handle:    b.handleUnCheckReleaseSlashCommand,
		SplitArgs: true,
		MinArgs:   3,
		MaxArgs:   3,
	})
	go func() {
		for !b.shouldStop {
			log.Infof("Starting webhook handler")
			if err := b.webhookHandler.Start(); err != nil {
				log.Error("Webhook handler fatal error: %s", err.Error())
			}
			if !b.shouldStop {
				log.Infof("Restarting webhook handler after failure")
			}
		}
	}()
}

func (b *Bot) handleCheckReleaseSlashCommand(query lm.SlashCommand, c echo.Context) error {
	log.Infof("Received command %s with args %v", query.Command, query.Args)
	releaseType := query.Args[0]
	releaseGroup := query.Args[1]
	releaseRepo := query.Args[2]

	switch releaseType {
	case "docker":
		ok, err := gDB.IsDockerHubImageRegistered(releaseGroup, releaseRepo)
		if err != nil {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text:         "Bad thing happen on my end, please see applications logs :(",
				ResponseType: "in_channel",
			})
		}

		if ok {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text: fmt.Sprintf("Docker image **%s/%s**, already checked, ignoring.",
					releaseGroup, releaseRepo),
				ResponseType: "in_channel",
			})
		}

		if !gDB.AddDockerHubImage(releaseGroup, releaseRepo) {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text:         "Bad thing happen on my end, please see applications logs :(",
				ResponseType: "in_channel",
			})
		}

		return c.JSON(200, lm.SlashCommandDirectResponse{
			Text: fmt.Sprintf("Docker image **%s/%s**, added to checklist !",
				releaseGroup, releaseRepo),
			ResponseType: "in_channel",
		})
	case "github":
		ok, err := gDB.IsGithubRepositoryRegistered(releaseGroup, releaseRepo)
		if err != nil {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text:         "Bad thing happen on my end, please see applications logs :(",
				ResponseType: "in_channel",
			})
		}

		if ok {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text: fmt.Sprintf("Github repository **%s/%s**, already checked, ignoring.",
					releaseGroup, releaseRepo),
				ResponseType: "in_channel",
			})
		}

		if !gDB.AddGithubRepository(releaseGroup, releaseRepo) {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text:         "Bad thing happen on my end, please see applications logs :(",
				ResponseType: "in_channel",
			})
		}

		return c.JSON(200, lm.SlashCommandDirectResponse{
			Text: fmt.Sprintf("Github repository **%s/%s**, added to checklist !",
				releaseGroup, releaseRepo),
			ResponseType: "in_channel",
		})
	default:
		return c.JSON(200, lm.SlashCommandDirectResponse{
			Text:         fmt.Sprintf("%s release mode is not know on our side.", releaseType),
			ResponseType: "in_channel",
		})
	}
}

func (b *Bot) handleUnCheckReleaseSlashCommand(query lm.SlashCommand, c echo.Context) error {
	log.Infof("Received command %s with args %v", query.Command, query.Args)
	releaseType := query.Args[0]
	releaseGroup := query.Args[1]
	releaseRepo := query.Args[2]

	switch releaseType {
	case "docker":
		ok, err := gDB.IsDockerHubImageRegistered(releaseGroup, releaseRepo)
		if err != nil {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text:         "Bad thing happen on my end, please see applications logs :(",
				ResponseType: "in_channel",
			})
		}

		if !ok {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text: fmt.Sprintf("Docker image **%s/%s**, is not registered, ignoring.",
					releaseGroup, releaseRepo),
				ResponseType: "in_channel",
			})
		}

		if !gDB.RemoveDockerHubImage(releaseGroup, releaseRepo) {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text:         "Bad thing happen on my end, please see applications logs :(",
				ResponseType: "in_channel",
			})
		}

		return c.JSON(200, lm.SlashCommandDirectResponse{
			Text: fmt.Sprintf("Docker image **%s/%s**, removed from checklist !",
				releaseGroup, releaseRepo),
			ResponseType: "in_channel",
		})
	case "github":
		ok, err := gDB.IsGithubRepositoryRegistered(releaseGroup, releaseRepo)
		if err != nil {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text:         "Bad thing happen on my end, please see applications logs :(",
				ResponseType: "in_channel",
			})
		}

		if !ok {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text: fmt.Sprintf("Github repository **%s/%s**, is not registered, ignoring.",
					releaseGroup, releaseRepo),
				ResponseType: "in_channel",
			})
		}

		if !gDB.RemoveGithubRepository(releaseGroup, releaseRepo) {
			return c.JSON(200, lm.SlashCommandDirectResponse{
				Text:         "Bad thing happen on my end, please see applications logs :(",
				ResponseType: "in_channel",
			})
		}

		return c.JSON(200, lm.SlashCommandDirectResponse{
			Text: fmt.Sprintf("Github repository **%s/%s**, removed from checklist !",
				releaseGroup, releaseRepo),
			ResponseType: "in_channel",
		})
	default:
		return c.JSON(200, lm.SlashCommandDirectResponse{
			Text:         fmt.Sprintf("%s release mode is not know on our side.", releaseType),
			ResponseType: "in_channel",
		})
	}
}

func (b *Bot) handleHealthcheck(c echo.Context) error {
	return c.JSON(200, lm.HealthcheckStatus{
		Status:  "ok",
		Message: "",
	})
}

func (b *Bot) handleReadinessCheck(c echo.Context) error {
	return c.JSON(200, lm.HealthcheckStatus{
		Status:  "ok",
		Message: "",
	})
}
