package internal

const (
	// ValidationQuery query used to validate connection
	ValidationQuery = `SELECT 1`
	// Github
	getGithubRepositories            = `SELECT gh_group, gh_name FROM github_repositories`
	updateGithubRepositoryLastUpdate = `UPDATE github_repositories SET last_update = NOW() WHERE gh_group = $1 AND gh_name = $2`
	isGithubRepositoryRegistered     = `SELECT EXISTS(SELECT 1 FROM github_repositories ` +
		`WHERE gh_group = $1 and gh_name = $2)`
	addGithubRepositoryQuery = `INSERT INTO github_repositories(gh_group, gh_name) VALUES ($1, $2) ON CONFLICT ` +
		`ON CONSTRAINT github_repositories_pkey DO NOTHING`
	removeGithubRepositoryQuery     = `DELETE FROM github_repositories WHERE gh_group = $1 AND gh_name = $2`
	addGithubRepositoryTag          = `INSERT INTO github_repository_tags (gh_group, gh_name, tag_name) VALUES ($1, $2, $3)`
	isGithubRepositoryTagRegistered = `SELECT EXISTS(SELECT 1 FROM github_repository_tags WHERE ` +
		`gh_group = $1 AND gh_name = $2 AND tag_name = $3);`

	// DockerHub
	getDockerHubImages             = `SELECT dh_group, dh_name FROM dockerhub_images WHERE last_update < NOW() - interval '1 hour'`
	updateDockerHubImageLastUpdate = `UPDATE dockerhub_images SET last_update = NOW() WHERE dh_group = $1 AND dh_name = $2`
	isDockerHubImageRegistered     = `SELECT EXISTS(SELECT 1 FROM dockerhub_images WHERE dh_group = $1 AND dh_name = $2)`
	addDockerHubImageQuery         = `INSERT INTO dockerhub_images(dh_group, dh_name) VALUES ($1, $2) ON CONFLICT ` +
		`ON CONSTRAINT dockerhub_images_pkey DO NOTHING`
	removeDockerHubImageQuery     = `DELETE FROM dockerhub_image WHERE dh_group = $1 AND dh_name = $2`
	addDockerHubImageTag          = `INSERT INTO dockerhub_image_tags (dh_group, dh_name, tag_name) VALUES ($1, $2, $3)`
	isDockerHubImageTagRegistered = `SELECT EXISTS(SELECT 1 FROM dockerhub_image_tags WHERE ` +
		`dh_group = $1 AND dh_name = $2 AND tag_name = $3);`
)
