ALTER TABLE github_repository_tags DROP CONSTRAINT github_repository_tags_gh_group_fkey,
	ADD CONSTRAINT github_repository_tags_gh_group_fkey FOREIGN KEY (gh_group, gh_name)
	   REFERENCES github_repositories ON UPDATE CASCADE;

ALTER TABLE dockerhub_image_tags DROP CONSTRAINT dockerhub_image_tags_dh_group_fkey,
	ADD CONSTRAINT dockerhub_image_tags_dh_group_fkey FOREIGN KEY (dh_group, dh_name)
 		REFERENCES dockerhub_images ON UPDATE CASCADE;