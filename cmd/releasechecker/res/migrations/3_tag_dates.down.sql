ALTER TABLE github_repositories DROP COLUMN registration_date;
ALTER TABLE github_repository_tags DROP COLUMN registration_date;
ALTER TABLE dockerhub_images DROP COLUMN registration_date;
ALTER TABLE dockerhub_image_tags DROP COLUMN registration_date;