module gitlab.com/nerzhul/releasechecker

require (
	github.com/docker/libtrust v0.0.0-20160708172513-aabc10ec26b7 // indirect
	github.com/golang-migrate/migrate/v4 v4.10.0
	github.com/google/go-github/v28 v28.1.1
	github.com/google/go-github/v30 v30.1.0
	github.com/heroku/docker-registry-client v0.0.0-20181004091502-47ecf50fd8d4
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/labstack/echo/v4 v4.1.15
	github.com/lib/pq v1.3.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/pborman/getopt v0.0.0-20190409184431-ee0cd42419d3 // indirect
	github.com/robfig/cron/v3 v3.0.1
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.5.1
	gitlab.com/nerzhul/libmatterbot v0.1.5
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	gopkg.in/yaml.v2 v2.2.8
)

go 1.13
